﻿using System.Net;
using Nancy.Json;
using Shop.Core.ServiceInterface;
using System.Threading.Tasks;
using Shop.Core.Dtos.Weather.OpenWeather;

namespace Shop.ApplicationServices.Services
{
    public class OpenWeatherServices : IOpenWeatherServices
    {

        public async Task<OpenWeatherResultDto> WeatherDetail(OpenWeatherResultDto dto)
        {
            string apikey = "00489edf71747bba4903648bddddd1a1";

            var City = dto.City.ToString();
            var url = $"http://api.openweathermap.org/data/2.5/weather?q=" + City + "&units=metric&APPID=" + apikey ;


            using (WebClient client = new WebClient())
            {
                string json = client.DownloadString(url);

                OpenWeatherRootDto weatherInfo = (new JavaScriptSerializer()).Deserialize<OpenWeatherRootDto>(json);

                dto.City = weatherInfo.Name;
                dto.MainTemp = weatherInfo.Main.Temp;
                dto.MainFeelsLike = weatherInfo.Main.Feels_Like;
                dto.Pressure = weatherInfo.Main.Pressure;
                dto.Humidity = weatherInfo.Main.Humidity;
                dto.WindSpeed = weatherInfo.Wind.Speed;
                dto.WeatherDescription = weatherInfo.Weather[0].Description;

                

                var jsonString = new JavaScriptSerializer().Serialize(dto);  
            }

         return dto;
        }

    }
}
