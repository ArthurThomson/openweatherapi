﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shop.Core.Dtos.Weather.OpenWeather
{
    public class WeatherDto
    {
        
      [JsonProperty("weather.description")]
      public string Description { get; set; }
      
    }
}
