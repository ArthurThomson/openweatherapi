﻿using Shop.Core.Dtos.Weather.OpenWeather;
using System.Threading.Tasks;


namespace Shop.Core.ServiceInterface
{
    public interface IOpenWeatherServices : IApplicationService
    {
        Task<OpenWeatherResultDto> WeatherDetail(OpenWeatherResultDto dto);

    }
}
