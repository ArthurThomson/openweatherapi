﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace Shop.Core.Dtos.Weather.OpenWeather
{
    public class OpenWeatherRootDto
    {
        
        public List<WeatherDto> Weather { get; set; }
        
        public Main Main { get; set; }
        
        public Wind Wind { get; set; }

        public string Name { get; set; }
             
    }
    

    public class Main
    {
        [JsonProperty("temp")]
        public double Temp { get; set; }
        [JsonProperty("feels_like")]
        public long Feels_Like { get; set; }
        [JsonProperty("temp_min")]
        public int TempMin { get; set; }
        [JsonProperty("temp_max")]
        public double TempMax { get; set; }
        [JsonProperty("pressure")]
        public int Pressure { get; set; }
        [JsonProperty("humidity")]
        public int Humidity { get; set; }
    }


    public class Wind
    {
        [JsonProperty("speed")]
        public double Speed { get; set; }
        [JsonProperty("deg")]
        public int Deg { get; set; }
    }
}
