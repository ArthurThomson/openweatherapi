﻿using Newtonsoft.Json;
using System;


namespace Shop.Core.Dtos.Weather.OpenWeather
{
    public class OpenWeatherResultDto
    {
        
        public string City { get; set; }
        
        public double MainTemp { get; set; }
        
        public Int64 MainFeelsLike { get; set; }
      
        public int Pressure { get; set; }
        
        public int Humidity { get; set; }
      
        public double WindSpeed { get; set; }
        
        public string WeatherDescription { get; set; }
    }
}
