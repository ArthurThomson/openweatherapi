﻿using Microsoft.AspNetCore.Mvc;
using Shop.Core.Dtos.Weather.OpenWeather;
using Shop.Core.ServiceInterface;
using Shop.Models.Weather;


namespace Shop.Controllers
{
    public class OpenWeatherController : Controller
    {
        private readonly IOpenWeatherServices _openweatherForecastServices;

        public OpenWeatherController
            (
                IOpenWeatherServices openweatherForecastServices
            )
        {
            _openweatherForecastServices = openweatherForecastServices;
        }

        [HttpGet]
        public IActionResult OpenWeatherSearchCity()
        {
            OpenWeatherSearchCity model = new OpenWeatherSearchCity();

            return View(model);
        }

        [HttpPost]
        public IActionResult OpenWeatherSearchCity(OpenWeatherSearchCity vm)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("OpenWeatherCity", "OpenWeather", new { City = vm.CityName });
            }

            return View(vm);
        }

        [HttpGet]
        public IActionResult OpenWeatherCity(string City)
        {
            
            OpenWeatherResultDto dto = new OpenWeatherResultDto();
            dto.City = City;
            _openweatherForecastServices.WeatherDetail(dto);
            OpenWeatherModel vm = new OpenWeatherModel();

            vm.City = dto.City;
            vm.MainTemp = dto.MainTemp;
            vm.MainFeelsLike = dto.MainFeelsLike;
            vm.Pressure = dto.Pressure;
            vm.Humidity = dto.Humidity;
            vm.WindSpeed = dto.WindSpeed;
            vm.WeatherDescription = dto.WeatherDescription;

            return View(vm);
        }
    }
}
